from preprocess import preprocess_whale
import numpy as np


def image_generator(files, label_file, batch_size=10):
    while True:
        # Select files (paths/indices) for the batch
        batch_paths = np.random.choice(a=files,
                                       size=batch_size)
        batch_input = []
        batch_output = []

        # Read in each input, perform preprocessing and get labels
        for input_path in batch_paths:
            for k in label_file:
                input = input_path
                #output = label_file

                input_p = preprocess_whale(input)
                batch_input += [input_p]
                batch_output += [k]
        # Return a tuple of (input,output) to feed the network
        batch_x = np.array(batch_input)#.resize(40, 224, 224, 3)

        batch_y = np.array(batch_output)#.resize(40, 224, 224, 3)
        print(batch_y.shape)

        yield (batch_x, batch_y)


def generator(features, labels, batch_size=10):

   # Create empty arrays to contain batch of features and labels#
   while True:
      batch_features = np.zeros((batch_size, 224, 224, 3))
      batch_labels = np.zeros((batch_size, 4250))
      for i in range(batch_size):
         # choose random index in features
         index= np.random.choice(len(features), 1)
         print("\n \n passed to preprocess whale \n \n", np.array(features)[index])
         batch_features[i] = preprocess_whale(np.array(features)[index])
         batch_labels[i] = labels[index]
         print("\n \n labels\n \n ", batch_labels)
      yield batch_features, batch_labels


def this_gen(files, label_file, batch_size=10):
    # Create empty arrays to contain batch of features and labels#
    while True:
        batch_features = np.zeros((batch_size, 224, 224, 3))
        batch_labels = np.zeros((batch_size, 4250))

        for i in range(batch_size):
            for image in files:
                # choose random index in features
                batch_features[i] = preprocess_whale(image)
                # index = np.where(image == batch_features)
                batch_labels = label_file[i]
            # for b in files:
            ##  index = (np.where(b==batch_paths))
            # t.append(index)

            # index= np.random.choice(len(features), 1)
            # print("\n \n passed to preprocess whale \n \n", batch_labels.shape)

        # batch_labels[i] = labels[index]
        # print("\n \n labels\n \n ", batch_labels)
        yield batch_features, batch_labels
