import pandas as pd
import joblib
import numpy as np
import math
import os
from numpy import asarray
from generator import image_generator, generator, this_gen
from convnet import model_convnet
from preprocess import preprocess_whale, encode_labels
from keras.preprocessing import image
from keras.models import Sequential
import gc
import keras
import tensorflow as tf


def get_session():
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    return tf.Session(config=config)


keras.backend.tensorflow_backend.set_session(get_session())

# load data

train_csv = pd.read_csv('../data/train.csv')
test_csv = pd.read_csv('../data/sample_submission.csv')
train_csv = train_csv[train_csv['Id'] != 'new_whale']
train_csv.reset_index(drop=True, inplace=True)
train_csv = train_csv.sample(frac=1) # lookup.


# Preprocess

train_image_list =[]
for i in range(len(train_csv)):
    path = os.path.join("../data/train/train/", train_csv['Image'][i])
    train_image_list.append( path)
train_id_list =[]
for i in range(len(train_csv)):
    train_id_list.append(train_csv['Id'][i])


# Splits
X_train_set = train_image_list[:5876]
Y_train_set = train_id_list[:5876]
X_holdout_set = train_image_list[5876:]
Y_holdout_set = train_id_list[5876:]

Y_whole_pre = train_id_list.copy()

# Preprocess cont
#print("Processing X_train and X_holdout")
#X_train = asarray(preprocess_whale(X_train_set ))
#X_holdout = asarray(preprocess_whale(X_holdout_set ))

print("\n \n Loading X processed data \n \n")
#joblib.dump(X_train, '../data/X_train.npy')
#joblib.dump(X_holdout, '../data/X_holdout.npy')
X_train = joblib.load('../data/X_train.npy')
X_holdout = joblib.load('../data/X_holdout.npy')


#print("Processing Y_train and Y_holdout")
#Y_train = asarray(encode_labels(Y_train_set))
#Y_holdout = asarray(encode_labels(Y_holdout_set))
#Y_whole = encode_labels(Y_whole_pre)
#Y_train = Y_whole[:5876]
#Y_holdout = Y_whole[5876:]

# Pickles
print("\n \n Loading Y processed data \n \n")

#joblib.dump(Y_whole, '../data/Y_whole.npy')
#joblib.dump(Y_train, '../data/Y_train.npy')
#joblib.dump(Y_holdout, '../data/Y_holdout.npy')
Y_train = joblib.load('../data/Y_train.npy')
Y_holdout = joblib.load('../data/Y_holdout.npy')


# Parameters
params = {'dim': (224, 224),
          'batch_size': 10,
          'n_classes': 4250,
          'n_channels': 3,
          'shuffle': True}

# Datasets
#partition = {}
#partition['train'] = X_train
#partition['validation'] = X_holdout

#X_whole = np.concatenate((X_train, X_holdout), axis=0)
#Y_whole = joblib.load(Y_whole, '../data/Y_whole.npy')
#labels = dict(zip(X_whole, Y_whole))

# Generators
#print("This is what's passed to the gen ", X_train_set)

training_generator = this_gen(X_train_set, Y_train)
validation_generator = this_gen(X_holdout_set, Y_holdout)

#gc.collect()
# Design model
print("\n \n Reading ConvNetModel \n \n ")
model = model_convnet()
# Train model on dataset
num_samples_train = X_train_set
num_samples_holdout = X_holdout_set
print("\n \n Training fit.generator \n \n")
model.fit_generator(generator=training_generator,
                    steps_per_epoch= (len(num_samples_train) //10 ),
                    #validation_data=validation_generator,
                  # validation_steps= (len(num_samples_holdout) // 10),
                    epochs=20, verbose=2, workers=1, use_multiprocessing =False)


print("\n \n Saving model pickle \n \n")

joblib.dump(model, '../data/generator_model.pkl')