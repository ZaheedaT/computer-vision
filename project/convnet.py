from keras import layers, models,optimizers
from keras.layers import Input, Add, Dense, Activation, ZeroPadding2D, BatchNormalization, Flatten, Conv2D
from keras.layers import AveragePooling2D, MaxPooling2D, Dropout, GlobalMaxPooling2D, GlobalAveragePooling2D
from keras.models import Model, load_model
from keras.wrappers.scikit_learn import KerasClassifier
from keras.initializers import glorot_uniform
from keras.preprocessing import image
from keras.utils import layer_utils
from keras.utils.data_utils import get_file
from keras.applications.imagenet_utils import preprocess_input
import pydot
from IPython.display import SVG
from keras.utils.vis_utils import model_to_dot
from keras.utils import plot_model
from keras.models import Sequential





def model_convnet(layers=1025, dropout=0.8, optimizor='Adam'):

    model = Sequential()

    model.add(Conv2D(32, (7, 7), strides=(1, 1), name='conv0', input_shape=( 224, 224, 3)))

    model.add(BatchNormalization(axis=3, name='bn0'))
    model.add(Activation('relu'))

    model.add(MaxPooling2D((2, 2), name='max_pool'))
    model.add(Conv2D(64, (3, 3), strides=(1, 1), name="conv1"))
    model.add(Activation('relu'))
    model.add(AveragePooling2D((3, 3), name='avg_pool'))

    model.add(Flatten())
    model.add(Dense(layers, activation="relu", name='rl'))
    model.add(Dropout(dropout))
    model.add(Dense(4250, activation='softmax', name='sm'))

    model.compile(loss='categorical_crossentropy', optimizer=optimizor, metrics=['accuracy'])
    print(model.summary())

    return model