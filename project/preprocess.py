import cv2
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
import random

def preprocess_whale(image_paths):
    """A function to preprocess image dataset.
    By resizing the images and turning them into arrays

    Parameters:
    m: int
        The training set size
    image_paths: string
        A string with the image paths of the dataset

    Returns:
    A 2D Numpy array
    """
    # X = np.zeros((m,100,100,3))
    # index = 0
    # for path in image_paths:
    # img = image.load_img(path, target_size=(100,100,3))
    # x = image.img_to_array(img)
    # x = preprocess_input(x)

   # X = []



    X = (cv2.resize(cv2.imread(image_paths, cv2.IMREAD_COLOR), (224, 224), interpolation=cv2.INTER_AREA)) # use 224, 224, 3 Dimensions

    # Initialize inpiut with zeros of desires dimensions
    # X[index] = x
    # index =+ 1

    return X


def encode_labels(y):

    y = pd.DataFrame(y)
    onehot_encoder = OneHotEncoder(sparse=False, categories='auto')
    y = y.values.reshape(len(y), 1) # matrix dimensions shape
    onehot_y = onehot_encoder.fit_transform(y)

    return onehot_y
