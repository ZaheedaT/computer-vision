#!/bin/bash
# setup.sh
# a bash script to setup the environment for the project

# install pip3 and virtual
sudo apt-get install python3-pip -y

# create the virutual environment in the project root
pip3 install virtualenv
virtualenv --no-site-packages -p python3 compvis_env 
source compvis_env/bin/activate
pip3 install -r requirements.txt
python -m ipykernel install --user --name compvis_kernel --display-name "Computer Vison"
