# Computer Vision

Ongoing 3 month Research under Telkom

## Data

Data may be found using this kaggle-api 
```sh
$ kaggle competitions download -c whale-categorization-playground
```

Or by going to this link  
https://www.kaggle.com/c/whale-categorization-playground/data
